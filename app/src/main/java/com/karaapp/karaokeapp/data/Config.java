package com.karaapp.karaokeapp.data;

/**
 * Created by Nganq on 4/24/2017.
 */

public class Config {

    private Config() {
    }
    public static final String YOUTUBE_API_KEY = "AIzaSyBMjRcOkq65odqDxxA-Cyw3UmPx6DHaCgs";
    public static final String SEARCH_API_KEY = "AIzaSyC25Dg71sjgbwgyL95BifqbU52kX4TSw3g";
    public static final String SHARED_PREFERENCE_NAME = "zenmo_share_preference";
    public static final String PROFILE_USER = "profile_user";
    public static  final String ROOT_FB_URL="https://www.facebook.com/";
    public static final String LIST_PUBLISH_PERMISSTION_FACEBOOK = "publish_actions";
    public static final boolean FLAG_DEBUG = true;
    public static final boolean DEBUG = false;
    public static final String ROOT_URL = "http://128.199.118.154:3002/";
    public static final String ROOT_URL_CHAT = "http://128.199.118.154:3000/";
    public static final String YOUTUBE_WATCH_URL_PREFIX = "http://www.youtube.com/watch?v=";
    public static final String fomatTime = "hh:mm:ss";
    public static final String fomatDateTime = "dd/MM/yyyy hh:mm:ss";
}
