package com.karaapp.karaokeapp.entity.facebook_friend;

import android.content.Context;

import com.google.gson.Gson;
import com.karaapp.karaokeapp.data.ZShPre;

import java.io.Serializable;
import java.util.Date;


public class ProfileUser implements Serializable {
    private String token;
    private Date timeExpires;
    private String idUser;
    private String displayName;
    private String email;

    private static ProfileUser instance = null;

    public static ProfileUser getIns(Context context) {
        if (instance == null) {
            instance = new Gson().fromJson(ZShPre.gIns(context).getProfileUser(),ProfileUser.class);
        }
        return instance;
    }

    public ProfileUser(){}

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getTimeExpires() {
        return timeExpires;
    }

    public void setTimeExpires(Date timeExpires) {
        this.timeExpires = timeExpires;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
