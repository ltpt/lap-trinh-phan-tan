
package com.karaapp.karaokeapp.entity.facebookinfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Comments {

    @SerializedName("data")
    @Expose
    private DataComment[] data = null;
    @SerializedName("paging")
    @Expose
    private PagingLike paging;

    public DataComment[] getData() {
        return data;
    }

    public void setData(DataComment[] data) {
        this.data = data;
    }

    public PagingLike getPaging() {
        return paging;
    }

    public void setPaging(PagingLike paging) {
        this.paging = paging;
    }

}
