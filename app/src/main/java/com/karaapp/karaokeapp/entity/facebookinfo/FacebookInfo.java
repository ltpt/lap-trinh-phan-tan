
package com.karaapp.karaokeapp.entity.facebookinfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FacebookInfo {

    @SerializedName("data")
    @Expose
    private DataSum[] data = null;
    @SerializedName("paging")
    @Expose
    private PagingSum paging;

    public DataSum[] getData() {
        return data;
    }

    public void setData(DataSum[] data) {
        this.data = data;
    }

    public PagingSum getPaging() {
        return paging;
    }

    public void setPaging(PagingSum paging) {
        this.paging = paging;
    }

}
