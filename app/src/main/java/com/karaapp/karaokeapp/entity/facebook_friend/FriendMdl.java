package com.karaapp.karaokeapp.entity.facebook_friend;

/**
 * Created by ThanhThuy on 01/07/2017.
 */

public class FriendMdl implements IFriendMdl {
    private String avatar;
    private String name;
    private boolean isCheck;

    public FriendMdl(String avatar, String name) {
        this.avatar = avatar;
        this.name = name;
    }

    public FriendMdl(String avatar, String name, boolean isCheck) {
        this.avatar = avatar;
        this.name = name;
        this.isCheck = isCheck;
    }

    @Override
    public String getAvatar() {
        return avatar;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean getCheck() {
        return isCheck;
    }

    @Override
    public void setCheck(boolean isCheck) {
        this.isCheck = isCheck;
    }
}
