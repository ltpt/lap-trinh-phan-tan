
package com.karaapp.karaokeapp.entity.facebookinfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PagingSum {

    @SerializedName("previous")
    @Expose
    private String previous;
    @SerializedName("next")
    @Expose
    private String next;

    public String getPrevious() {
        return previous;
    }

    public void setPrevious(String previous) {
        this.previous = previous;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

}
