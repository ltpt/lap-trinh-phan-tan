
package com.karaapp.karaokeapp.entity.facebookinfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataShare {

    @SerializedName("created_time")
    @Expose
    private String createdTime;
    @SerializedName("story")
    @Expose
    private String story;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("message")
    @Expose
    private String message;

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
