
package com.karaapp.karaokeapp.entity.facebookinfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Sharedposts {

    @SerializedName("data")
    @Expose
    private DataShare[] data = null;
    @SerializedName("paging")
    @Expose
    private PagingShare paging;

    public DataShare[] getData() {
        return data;
    }

    public void setData(DataShare[] data) {
        this.data = data;
    }

    public PagingShare getPaging() {
        return paging;
    }

    public void setPaging(PagingShare paging) {
        this.paging = paging;
    }

}
