
package com.karaapp.karaokeapp.entity.facebookinfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Likes {

    @SerializedName("data")
    @Expose
    private DataLike[] data = null;
    @SerializedName("paging")
    @Expose
    private PagingLike paging;

    public DataLike[] getData() {
        return data;
    }

    public void setData(DataLike[] data) {
        this.data = data;
    }

    public PagingLike getPaging() {
        return paging;
    }

    public void setPaging(PagingLike paging) {
        this.paging = paging;
    }

}
