package com.karaapp.karaokeapp.entity.facebook_friend;

/**
 * Created by ThanhThuy on 03/07/2017.
 */

public class Data {
    private String id;
    private String name;
    private Picture picture;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Picture getPicture() {
        return picture;
    }

    public void setPicture(Picture picture) {
        this.picture = picture;
    }

}
