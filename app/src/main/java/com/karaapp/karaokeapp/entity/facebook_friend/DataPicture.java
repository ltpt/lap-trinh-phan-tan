package com.karaapp.karaokeapp.entity.facebook_friend;

/**
 * Created by ThanhThuy on 03/07/2017.
 */

public class DataPicture {
    private boolean is_silhouette;
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
