package com.karaapp.karaokeapp.entity.facebook_friend;

/**
 * Created by ThanhThuy on 01/07/2017.
 */

public interface IFriendMdl {
    String getAvatar();
    String getName();
    boolean getCheck();

    void setCheck(boolean isCheck);
}
