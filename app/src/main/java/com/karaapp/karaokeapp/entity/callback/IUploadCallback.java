package com.karaapp.karaokeapp.entity.callback;

import java.util.List;

/**
 * Created com.karaapp.karaokeapp.entity.callback on 9/2/2017.
 * Author: NgaNQ
 */
public interface IUploadCallback {
    void callback(String StringParamUpload);

}
