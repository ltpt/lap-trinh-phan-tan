package com.karaapp.karaokeapp.uploadvideo;

import android.os.Bundle;
import android.util.Log;

import com.facebook.FacebookBroadcastReceiver;
import com.karaapp.karaokeapp.data.Config;

/**
 * Created package com.karaapp.karaokeapp.uploadvideo on 04/06/2017.
 */

public class UploadVideoBroadcastReceiver extends FacebookBroadcastReceiver {

    @Override
    protected void onSuccessfulAppCall(String appCallId, String action, Bundle extras) {
        // A real app could update UI or
        // notify the user that their photo was uploaded.
        if (Config.FLAG_DEBUG){
            Log.d("UploadVideo", String.format("Photo uploaded by call: " + appCallId + " succeeded."));
        }
    }

    @Override
    protected void onFailedAppCall(String appCallId, String action, Bundle extras) {
        // A real app could update UI or notify the user that their photo was not uploaded.
        if (Config.FLAG_DEBUG) {
            Log.d("UploadVideo", String.format("Photo uploaded by call " + appCallId + " failed."));
        }
    }

}
