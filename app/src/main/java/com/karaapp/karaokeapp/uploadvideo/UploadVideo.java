package com.karaapp.karaokeapp.uploadvideo;

import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.share.Sharer;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.model.ShareVideoContent;
import com.facebook.share.widget.ShareDialog;
import com.google.gson.Gson;
import com.karaapp.karaokeapp.R;
import com.karaapp.karaokeapp.data.Config;
import com.karaapp.karaokeapp.ui.activity.MainActivity;
import com.karaapp.karaokeapp.ui.dialog.CustomProgressDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;

import static android.content.Context.MODE_PRIVATE;
import static com.karaapp.karaokeapp.data.Config.fomatDateTime;

/**
 * Created package com.karaapp.karaokeapp.uploadvideo on 03/06/2017.
 */

public class UploadVideo {
    private int count = 0;
    private String matchURL;
    private int bufferSize = 50000;
    private byte[] data = null;
    private boolean canPresentShareDialogWithVideos;
    private boolean canPresentShareDialogWithPhoto;
    private MainActivity activity;
    private static final String PERMISSION = "publish_actions";
    private Uri uri;
    private String id;
    private String source, picture, contentShare, createdTime;
    private Gson gson;

    private static final Location SEATTLE_LOCATION = new Location("") {
        {
            setLatitude(47.6097);
            setLongitude(-122.3331);
        }
    };
    private final String PENDING_ACTION_BUNDLE_KEY =
            "com.karaapp.karaokeapp.uploadvideo:PendingAction";
    private CallbackManager callbackManager = CallbackManager.Factory.create();
    private ShareDialog shareDialog;
    private PendingAction pendingAction = PendingAction.NONE;

    private FacebookCallback<Sharer.Result> fbCallback;


    private enum PendingAction {
        NONE,
        POST_VIDEO
    }

    private FacebookCallback<Sharer.Result> shareCallback =
            new FacebookCallback<Sharer.Result>() {
                @Override
                public void onSuccess(Sharer.Result result) {
                    if (Config.FLAG_DEBUG) {
                        Log.d(activity.getResources().getString(R.string.upload), activity.getResources().getString(R.string.success));
                    }

                    if (result.getPostId() != null) {
                        String title = activity.getString(R.string.success);
                        String id = result.getPostId();
                        String alertMessage = activity.getString(R.string.successfully_posted_post, id);
                        showResult(title, alertMessage);
                    }
                }

                @Override
                public void onCancel() {
                    if (Config.FLAG_DEBUG) {
                        Log.d("UploadVideo", "Cancel");
                    }
                }

                @Override
                public void onError(FacebookException error) {
                    if (Config.FLAG_DEBUG) {
                        Log.d("UploadVideo", "Error");
                    }
                    String title = activity.getString(R.string.error);
                    String alertMessage = error.getMessage();
                    showResult(title, alertMessage);
                }

                private void showResult(String title, String alertMessage) {
                    new AlertDialog.Builder(activity)
                            .setTitle(title)
                            .setMessage(alertMessage)
                            .setPositiveButton(R.string.OK, null)
                            .show();
                }
            };


    public UploadVideo(MainActivity activity) {
        this.activity = activity;
        /** auto login xác thực người dùng  */
        AutoLogin();
    }

    private void AutoLogin() {
        shareDialog = new ShareDialog(activity);
        shareDialog.registerCallback(callbackManager, shareCallback);
        canPresentShareDialogWithPhoto = ShareDialog.canShow(SharePhotoContent.class);
        canPresentShareDialogWithVideos = ShareDialog.canShow(ShareVideoContent.class);
    }

    private boolean hasPublishPermission() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null && accessToken.getPermissions().contains("publish_actions");
    }


    private byte[] readBytes(String dataPath) throws IOException {
        InputStream inputStream = new FileInputStream(new File(dataPath));
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        byte[] buffer = new byte[bufferSize];
        int length;
        while ((length = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, length);
            count++; // Số lần đọc file nhị phân từ file nhạc

        }
        return byteBuffer.toByteArray();
    }

    public void shareVideoFB(String videoPath,CustomProgressDialog customProgressDialog, String idSong) {
        customProgressDialog.show();
        customProgressDialog.setCancelable(false);
        customProgressDialog.setCanceledOnTouchOutside(false);
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        String fileName = videoPath.substring(videoPath.lastIndexOf('/') + 1, videoPath.length());
        try {
            data = readBytes(videoPath);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String privacy = getPrivacy("Everyone");
        GraphRequest request = GraphRequest.newPostRequest(accessToken, "/me/videos", null, new GraphRequest.Callback() {
            @Override
            public void onCompleted(GraphResponse response) {

                String newsFeed = response.getRawResponse();
                try {
                    JSONObject jsonObj = new JSONObject(newsFeed);
                    id = jsonObj.getString("id");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                requestUpdate(idSong);
                customProgressDialog.dismiss();
            }
        });
        Bundle params = request.getParameters();
        params.putString("title", fileName.replace("_", " "));
        params.putString("description", "Karaoke App " + fileName.replace("_", " ") + "  https://play.google.com/store/apps/details?id=com.pqsoftware.karaoke");
        params.putByteArray(fileName, data);
        params.putInt("file_size", data.length);

        request.setParameters(params);
        request.executeAsync();

    }

    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }

    private void requestUpdate(String idSong) {
        SharedPreferences preferences = activity.getSharedPreferences("my_data", MODE_PRIVATE);
        String bchk = preferences.getString("idProfile", "");
        if (bchk != null) {
            matchURL = "/" + id;
        }
        GraphRequest request1 = GraphRequest.newGraphPathRequest(
                AccessToken.getCurrentAccessToken(),
                matchURL,
                new GraphRequest.Callback() {
                    @Override
                    public void onCompleted(GraphResponse response) {
                        String newsRequest = response.getRawResponse();
                        try {
                            JSONObject jsonObj = new JSONObject(newsRequest);
                            source = jsonObj.getString("permalink_url");
                            picture = jsonObj.getString("picture");
                            contentShare = jsonObj.getString("description");
                            SimpleDateFormat dateFormat = new SimpleDateFormat(fomatDateTime);
                            Date date = new Date(System.currentTimeMillis());
                            createdTime = dateFormat.format(date);
                            try {
                                source = URLEncoder.encode(source, "UTF-8");
                                picture = URLEncoder.encode(picture, "UTF-8");
                                contentShare = URLEncoder.encode(contentShare, "UTF-8");
                                createdTime = URLEncoder.encode(createdTime, "UTF-8");

                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }

                            UpdateDataStringRequest(Config.ROOT_URL + "updateSong?idPost=" + id + "&idProfile=" + bchk + "&source=" + source + "&content_share=" + contentShare + "&createdTime=" + createdTime + "&idSong=" + idSong);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.i("Upload", newsRequest + " " + source + " " + picture + " " + " " + contentShare);
                    }
                });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "description,picture,created_time,permalink_url");
        request1.setParameters(parameters);
        request1.executeAsync();
    }

    private static String getPrivacy(String privacy) {
        String str;
        if (privacy.equalsIgnoreCase("Everyone"))
            str = "EVERYONE";
        if (privacy.equalsIgnoreCase("Friends and Networks"))
            str = "NETWORKS_FRIENDS";
        else if (privacy.equalsIgnoreCase("Friends of Friends"))
            str = "FRIENDS_OF_FRIENDS";
        else if (privacy.equalsIgnoreCase("Friends Only"))
            str = "ALL_FRIENDS";
        else if (privacy.equalsIgnoreCase("Custom"))
            str = "CUSTOM";
        else if (privacy.equalsIgnoreCase("Specific People..."))
            str = "SOME_FRIENDS";
        else
            str = "SELF";

        return str;
    }

    private void UpdateDataStringRequest(String url) {
        RequestQueue requestQueue = Volley.newRequestQueue(activity);

        StringRequest mStringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(activity, "Bạn đã chia sẻ một file lên facebook thành công ", Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(activity, "Bạn đã chia sẻ một file lên facebook thành công ", Toast.LENGTH_SHORT).show();
            }
        });
        requestQueue.add(mStringRequest);

    }

}