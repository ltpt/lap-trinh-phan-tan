package com.karaapp.karaokeapp.common;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;

import android.content.Intent;
import com.karaapp.karaokeapp.ui.dialog.InternetOffDialog;

/**
 * Created com.karaapp.karaokeapp.common on 8/8/2017.
 * Author: NgaNQ
 */
public class NetworkChangeReceiver extends BroadcastReceiver {

    public boolean isConnected = true;
    private String status;
    private Context Cnt;
    private Activity activity;
    private Activity parent;
    private AlertDialog alert;



    @Override
    public void onReceive(final Context context, final Intent intent) {

        activity = (Activity) context;
        status = NetworkUtil.getConnectivityStatusString(context);
        /*if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
            Toast.makeText(context, "Internet connection ", Toast.LENGTH_LONG).show();
        }
        else
            Toast.makeText(context, "Internet connection fail", Toast.LENGTH_LONG).show();*/
        ReturnStatus(status, context);
    }

    public void ReturnStatus(String s, final Context cnt) {
        if (s.equals("Mobile data enabled")) {
            isConnected = true;
        } else if (s.equals("Wifi enabled")) {
            isConnected = true;
        } else {
            isConnected = false;

            InternetOffDialog dialog = new InternetOffDialog(activity,isConnected);
            dialog.show();
        }
    }

    public boolean is_connected() {
        return isConnected;
    }
}
