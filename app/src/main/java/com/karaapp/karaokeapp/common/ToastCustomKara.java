package com.karaapp.karaokeapp.common;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.karaapp.karaokeapp.R;

/**
 * Created com.karaapp.karaokeapp.common on 8/23/2017.
 * Author: NgaNQ
 */
public class ToastCustomKara {
    public static Toast makeText(Context context, String msg, int length, int type) {
        Toast toast = new Toast(context);
     switch (type){
        case 1: {
        View layout = LayoutInflater.from(context).inflate(R.layout.error_toast_layout, null, false);
            TextView text = (TextView) layout.findViewById(R.id.txt_messages);
            text.setText(msg);
            text.setBackgroundResource(R.drawable.toast_failed);
            text.setTextColor(Color.parseColor("#FFFFFF"));
            text.setCompoundDrawablesWithIntrinsicBounds(R.drawable.bingo,0,0,0);
            toast.setView(layout);
            break;
        }
        case 2: {
            View layout = LayoutInflater.from(context).inflate(R.layout.succes_toast_layout, null, false);
            TextView text = (TextView) layout.findViewById(R.id.txt_messages_success);
            text.setText(msg);
            text.setBackgroundResource(R.drawable.toast_success);
            text.setTextColor(Color.parseColor("#FFFFFF"));
            toast.setView(layout);
            break;
        }
    }
        toast.setDuration(length);
        toast.show();
        return toast;
    }
}
