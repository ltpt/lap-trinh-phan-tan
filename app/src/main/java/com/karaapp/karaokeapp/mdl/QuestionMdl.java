package com.karaapp.karaokeapp.mdl;

/**
 * Created com.karaapp.karaokeapp.ui.Mdl on 5/3/2017.
 * Author: NgaNQ
 */

public class QuestionMdl {
    private String reply;
    public QuestionMdl(int numbers, String question) {
        this.numbers = numbers;
        this.question = question;
    }

    public QuestionMdl(int numbers, String question, String reply){
        this.numbers = numbers;
        this.question = question;
        this.reply = reply;
    }
    public QuestionMdl() {
    }

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }

    public int getNumbers() {

        return numbers;
    }

    public QuestionMdl(String reply) {
        this.reply = reply;
    }

    public void setNumbers(int numbers) {
        this.numbers = numbers;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    private int numbers;
    private String question;
}
