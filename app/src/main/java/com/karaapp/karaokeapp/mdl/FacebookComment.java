package com.karaapp.karaokeapp.mdl;

/**
 * Created com.karaapp.karaokeapp.mdl on 8/6/2017.
 * Author: NgaNQ
 */
public class FacebookComment implements IFacebookComment{
    private String profleID;
    private String comment;
    private String createdTime;
    private String username;

    public FacebookComment(String profleID, String comment, String createdTime, String username) {
        this.profleID = profleID;
        this.comment = comment;
        this.createdTime = createdTime;
        this.username = username;
    }
    @Override
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    @Override
    public String getProfleID() {
        return profleID;
    }

    public void setProfleID(String profleID) {
        this.profleID = profleID;
    }
    @Override
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
    @Override
    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }
}
