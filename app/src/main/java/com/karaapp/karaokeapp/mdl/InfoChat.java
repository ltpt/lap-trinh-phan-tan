package com.karaapp.karaokeapp.mdl;

/**
 * Created com.karaapp.karaokeapp.mdl on 7/3/2017.
 * Author: NgaNQ
 */
public class InfoChat {
    public static final int TYPE_MESSAGE = 0;
    public static final int TYPE_LOG = 1;
    public static final int TYPE_ACTION = 2;


    private int mType;
    private String idProfile;
    private String nameProfile;
    private String messages;

    public InfoChat(int mType, String idProfile, String nameProfile, String messages) {
        this.mType = mType;
        this.idProfile = idProfile;
        this.nameProfile = nameProfile;
        this.messages = messages;
    }




    public int getmType() {
        return mType;
    }

    public void setmType(int mType) {
        this.mType = mType;
    }
    public String getFacebookID() {
        return idProfile;
    }

    public void setFacebookID(String facebookID) {
        this.idProfile = facebookID;
    }

    public String getName() {
        return nameProfile;
    }

    public void setName(String name) {
        this.nameProfile = name;
    }

    public String getMessages() {
        return messages;
    }

    public void setMessages(String messages) {
        this.messages = messages;
    }
}
