package com.karaapp.karaokeapp.mdl;

import java.io.Serializable;

/**
 * Created by Nganq on 4/18/2017.
 */

public class VideoMdl implements Serializable{

    private String videoId;
    private String title;
    private String url;
    private String durations;

    public VideoMdl() {
        this.videoId = "";
        this.title = "";
        this.url = "";
        this.durations = "";
    }

    public String getViewCount() {
        return viewCount;
    }

    public void setViewCount(String viewCount) {
        this.viewCount = viewCount;
    }

    private String viewCount;

    public String getDurations() {
        return durations;
    }

    public void setDurations(String durations) {
        this.durations = durations;
    }

    public VideoMdl(String videoId, String title, String url) {
        this.videoId = videoId;
        this.title = title;
        this.url = url;
    }

    public VideoMdl(String videoId, String title, String url, String durations) {
        this.videoId = videoId;
        this.title = title;
        this.url = url;
        this.durations = durations;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}

