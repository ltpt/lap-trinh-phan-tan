package com.karaapp.karaokeapp.mdl;

/**
 * Created com.karaapp.karaokeapp.mdl on 8/28/2017.
 * Author: NgaNQ
 */
public class ViewCount {
    private String likeInApp;

    public String getLikeInApp ()
    {
        return likeInApp;
    }

    public void setLikeInApp (String likeInApp)
    {
        this.likeInApp = likeInApp;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [likeInApp = "+likeInApp+"]";
    }
}
