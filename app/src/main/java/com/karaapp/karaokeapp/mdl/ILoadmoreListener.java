package com.karaapp.karaokeapp.mdl;

/**
 * Created com.karaapp.karaokeapp.mdl on 7/18/2017.
 * Author: NgaNQ
 */
public interface ILoadmoreListener {
    public void loadData();
}
