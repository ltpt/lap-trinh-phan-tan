package com.karaapp.karaokeapp.mdl;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created com.karaapp.karaokeapp.mdl on 7/14/2017.
 * Author: NgaNQ
 */
public class MessagesRespone implements Parcelable {
    private String msg;

    public String getMsg ()
    {
        return msg;
    }

    public void setMsg (String msg)
    {
        this.msg = msg;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [msg = "+msg+"]";
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.msg);
    }

    public MessagesRespone() {
    }

    protected MessagesRespone(Parcel in) {
        this.msg = in.readString();
    }

    public static final Creator<MessagesRespone> CREATOR = new Creator<MessagesRespone>() {
        @Override
        public MessagesRespone createFromParcel(Parcel source) {
            return new MessagesRespone(source);
        }

        @Override
        public MessagesRespone[] newArray(int size) {
            return new MessagesRespone[size];
        }
    };
}
