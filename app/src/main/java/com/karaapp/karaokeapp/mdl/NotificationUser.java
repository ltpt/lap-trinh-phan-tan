package com.karaapp.karaokeapp.mdl;

/**
 * Created com.karaapp.karaokeapp.mdl on 5/18/2017.
 * Author: NgaNQ
 */

public class NotificationUser implements INotificationUser {
    private String idnoti;

    private String createdTime;

    private String status;

    private String pictureLink;

    private String idSong;

    private String type;

    private String size;

    private String point;

    private String createdTimeSend;

    private String titleSong;

    private String duration;

    private String idProfile;

    private String nameProfile;

    private String likeInApp;

    private String path;

    private String isUpload;

    private String idPost;

    public String getIdnoti ()
    {
        return idnoti;
    }

    public void setIdnoti (String idnoti)
    {
        this.idnoti = idnoti;
    }

    public String getCreatedTime ()
    {
        return createdTime;
    }

    public void setCreatedTime (String createdTime)
    {
        this.createdTime = createdTime;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getPictureLink ()
    {
        return pictureLink;
    }

    public void setPictureLink (String pictureLink)
    {
        this.pictureLink = pictureLink;
    }

    public String getIdSong ()
    {
        return idSong;
    }

    public void setIdSong (String idSong)
    {
        this.idSong = idSong;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    public String getSize ()
    {
        return size;
    }

    public void setSize (String size)
    {
        this.size = size;
    }

    public String getPoint ()
    {
        return point;
    }

    public void setPoint (String point)
    {
        this.point = point;
    }

    public String getCreatedTimeSend ()
    {
        return createdTimeSend;
    }

    public void setCreatedTimeSend (String createdTimeSend)
    {
        this.createdTimeSend = createdTimeSend;
    }

    public String getTitle ()
    {
        return titleSong;
    }

    public void setTitle (String titleSong)
    {
        this.titleSong = titleSong;
    }

    public String getDuration ()
    {
        return duration;
    }

    public void setDuration (String duration)
    {
        this.duration = duration;
    }

    public String getIdProfile ()
    {
        return idProfile;
    }

    public void setIdProfile (String idProfile)
    {
        this.idProfile = idProfile;
    }

    public String getNameProfile ()
    {
        return nameProfile;
    }

    public void setNameProfile (String nameProfile)
    {
        this.nameProfile = nameProfile;
    }

    public String getLikeInApp ()
    {
        return likeInApp;
    }

    public void setLikeInApp (String likeInApp)
    {
        this.likeInApp = likeInApp;
    }

    public String getPath ()
    {
        return path;
    }

    public void setPath (String path)
    {
        this.path = path;
    }

    public String getIsUpload ()
    {
        return isUpload;
    }

    public void setIsUpload (String isUpload)
    {
        this.isUpload = isUpload;
    }

    public String getIdPost ()
    {
        return idPost;
    }

    public void setIdPost (String idPost)
    {
        this.idPost = idPost;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [idnoti = "+idnoti+", createdTime = "+createdTime+", status = "+status+", pictureLink = "+pictureLink+", idSong = "+idSong+", type = "+type+", size = "+size+", point = "+point+", createdTimeSend = "+createdTimeSend+", title = "+titleSong+", duration = "+duration+", idProfile = "+idProfile+", nameProfile = "+nameProfile+", likeInApp = "+likeInApp+", path = "+path+", isUpload = "+isUpload+", idPost = "+idPost+"]";
    }
}
