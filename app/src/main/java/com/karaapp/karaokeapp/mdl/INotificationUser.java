package com.karaapp.karaokeapp.mdl;

/**
 * Created com.karaapp.karaokeapp.mdl on 7/17/2017.
 * Author: NgaNQ
 */
public interface INotificationUser {

    public String getCreatedTimeSend ();

    public String getTitle ();

    public String getNameProfile ();

}
