package com.karaapp.karaokeapp.mdl;

/**
 * Created com.karaapp.karaokeapp.mdl on 8/9/2017.
 * Author: NgaNQ
 */
public interface IFacebookComment {
    String getUsername();
    String getProfleID();
    String getComment();
    String getCreatedTime();
}
