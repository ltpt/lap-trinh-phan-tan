package com.karaapp.karaokeapp.mdl;

/**
 * Created com.karaapp.karaokeapp.mdl on 7/17/2017.
 * Author: NgaNQ
 */
public interface IRecordVidMdl {
    public void setPath(String path);

    public String getIsUpload();

    public String getDurationVid();

    public String getIdPost() ;

    public String getTitle() ;

    public String getPath() ;

    public String getDuration() ;

    public String getPictureLink();

    public String getCreatedTime() ;

    public String getSize() ;

    public String getPoint();

    public String getLikeInApp();

    public String getIdSong();
}
