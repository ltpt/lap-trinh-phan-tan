package com.karaapp.karaokeapp.mdl;

import java.io.Serializable;

/**
 * Created com.karaapp.karaokeapp.mdl on 5/5/2017.
 * Author: NgaNQ
 */

public class RecordVidMdl implements IRecordVidMdl {
    private String idSong;
    private String idPost;
    private String titleSong;
    private String path;
    private String isUpload;
    private String duration;
    private String pictureLink;
    private String createdTime;
    private String size;
    private String point;
    private String type;
    private String idProfile;
    private String likeInApp;
    private String nameProfile;
    private String durationVid;
    private String content_share;
    private String source;

    public String getContent_share() {
        return content_share;
    }

    public void setContent_share(String content_share) {
        this.content_share = content_share;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDurationVid() {
        return durationVid;
    }

    public void setDurationVid(String durationVid) {
        this.durationVid = durationVid;
    }

    public String getIdSong() {
        return idSong;
    }

    public void setIdSong(String idSong) {
        this.idSong = idSong;
    }

    public String getIdPost() {
        return idPost;
    }

    public void setIdPost(String idPost) {
        this.idPost = idPost;
    }

    public String getTitle() {
        return titleSong;
    }

    public void setTitle(String title) {
        this.titleSong = title;
    }

    public String getPath() {
        return path;
    }
    @Override
    public void setPath(String path) {
        this.path = path;
    }

    public String getIsUpload() {
        return isUpload;
    }

    public void setIsUpload(String isUpload) {
        this.isUpload = isUpload;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getPictureLink() {
        return pictureLink;
    }

    public void setPictureLink(String pictureLink) {
        this.pictureLink = pictureLink;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIdProfile() {
        return idProfile;
    }

    public void setIdProfile(String idProfile) {
        this.idProfile = idProfile;
    }

    public String getLikeInApp() {
        return likeInApp;
    }

    public void setLikeInApp(String likeInApp) {
        this.likeInApp = likeInApp;
    }

    public String getNameProfile() {
        return nameProfile;
    }

    public void setNameProfile(String nameProfile) {
        this.nameProfile = nameProfile;
    }


}
