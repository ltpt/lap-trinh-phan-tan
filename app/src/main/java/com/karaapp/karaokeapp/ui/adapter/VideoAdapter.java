package com.karaapp.karaokeapp.ui.adapter;

import android.database.Cursor;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPopupHelper;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.karaapp.karaokeapp.mdl.VideoMdl;
import com.karaapp.karaokeapp.R;
import com.karaapp.karaokeapp.ui.activity.MainActivity;

import java.util.List;

import static com.karaapp.karaokeapp.ui.activity.MainActivity.databaseKara;

/**
 * Created by Nganq on 4/8/2017.
 */

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.VideoViewHolder> {
    private List<VideoMdl> mListVideo;
    private static OnItemClickListener listener;
    private boolean isClick = false;
    private MainActivity activity;

    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public VideoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_video, parent, false);
        activity = (MainActivity) parent.getContext();

        return new VideoViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(VideoViewHolder holder, int position) {
        VideoMdl mVideo = mListVideo.get(position);
        holder.txtTittle.setText(mVideo.getTitle());
        Glide.with(holder.imgVideo.getContext()).load(mVideo.getUrl()).into(holder.imgVideo);
        holder.txtDuration.setText(mVideo.getDurations());
        holder.save_love.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupMenu(holder.save_love, position);
            }
        });
    }

    private void showPopupMenu(View view, int position) {
        PopupMenu popupMenu = new PopupMenu(view.getContext(), view);
        MenuInflater inflater = popupMenu.getMenuInflater();
        inflater.inflate(R.menu.save_love, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(new VideoAdapter.MyMenuItemClickListener(position));
        popupMenu.show();

        MenuPopupHelper menuHelper = new MenuPopupHelper(view.getContext(), (MenuBuilder) popupMenu.getMenu(), view);
        menuHelper.setForceShowIcon(true);
        menuHelper.show();

    }

    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

        private int position;

        public MyMenuItemClickListener(int position) {
            this.position = position;
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()) {
                /*case R.id.item_save_love:
                    Cursor dataListFavourite = databaseKara.getData("SELECT * FROM ListFavourite WHERE videoID = '" + mListVideo.get(position).getVideoId() + "'");
                    if (dataListFavourite.moveToNext()) {
                        Toast.makeText(activity, R.string.exist_favourite, Toast.LENGTH_SHORT).show();
                    } else
                        databaseKara.queryData(
                                "INSERT INTO ListFavourite SELECT * FROM (SELECT  " + "\"" + mListVideo.get(position).getVideoId() + "\",\"" + mListVideo.get(position).getTitle() + "\",\"" + mListVideo.get(position).getDurations() +
                                        "\",\"" + mListVideo.get(position).getUrl() + "\",\"" + "KARAOKE CHỌN BÀI" + "\")" +
                                        "AS tmp WHERE NOT EXISTS ( SELECT videoID FROM ListFavourite WHERE videoID = '" + mListVideo.get(position).getVideoId() + "' ) LIMIT 1");

                    break;
                */default:
                    isClick = false;
            }
            return false;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return mListVideo.size();
    }

    public VideoAdapter(List<VideoMdl> mListVideo) {
        this.mListVideo = mListVideo;


    }

    public class VideoViewHolder extends RecyclerView.ViewHolder {
        ImageView imgVideo;
        TextView txtTittle;
        TextView txtDuration;
        Button save_love;

        public VideoViewHolder(final View itemView) {
            super(itemView);
            imgVideo = (ImageView) itemView.findViewById(R.id.image);
            txtTittle = (TextView) itemView.findViewById(R.id.title);
            txtDuration = (TextView) itemView.findViewById(R.id.txtdurations);
            save_love = (Button) itemView.findViewById(R.id.btn);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null)
                        listener.onItemClick(itemView, getLayoutPosition());
                }
            });


        }

    }
}
