package com.karaapp.karaokeapp.ui.dialog;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.StyleRes;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.karaapp.karaokeapp.R;
import com.karaapp.karaokeapp.data.Resource;
import com.karaapp.karaokeapp.ui.activity.MainActivity;

/**
 * Created com.karaapp.karaokeapp.ui.dialog on 8/25/2017.
 * Author: NgaNQ
 */
public class InternetOffDialog extends KaraDialog {
    private MainActivity activity;
    private boolean isConnectInDialog;
    public InternetOffDialog(@NonNull Context context,boolean isConnected) {
        super(context);
        this.activity = (MainActivity) context;
        isConnectInDialog=isConnected;

    }

    public InternetOffDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        this.activity = (MainActivity) context;

    }

    @Override
    public View generateContent(LinearLayout linearLayout, LayoutInflater layoutInflater) {
        View view = layoutInflater.inflate(R.layout.shutdown_content,
                linearLayout);
        TextView tvContent = (TextView) view.findViewById(R.id.shutdown_content);
        tvContent.setText(Resource.getInstance(activity).getString(R.string.turn_on_wifi_kara));
        return view;
    }

    @Override
    public View generateFooter(LinearLayout linearLayout, LayoutInflater layoutInflater) {
        View view = layoutInflater.inflate(R.layout.shutdown_footer, linearLayout);
        Button btConfirm = (Button) view.findViewById(R.id.bt_confirm);
        Button btCancel = (Button) view.findViewById(R.id.bt_cancel);

        btCancel.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                isConnectInDialog=false;
                InternetOffDialog.this.dismiss();
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(1);
            }
        });
        btConfirm.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                isConnectInDialog=false;
                InternetOffDialog.this.dismiss();
                activity.finish();
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.setClassName("com.android.settings", "com.android.settings.wifi.WifiSettings");
                activity.startActivity(intent);
            }
        });

        return view;
    }
}
