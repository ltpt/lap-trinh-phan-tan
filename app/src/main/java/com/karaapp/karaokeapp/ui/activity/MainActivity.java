package com.karaapp.karaokeapp.ui.activity;

import android.animation.ObjectAnimator;
import android.app.SearchManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.SearchView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.LoggingBehavior;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.gson.Gson;
import com.karaapp.karaokeapp.R;
import com.karaapp.karaokeapp.common.NetworkChangeReceiver;
import com.karaapp.karaokeapp.data.Config;
import com.karaapp.karaokeapp.data.DatabaseKara;
import com.karaapp.karaokeapp.data.Resource;
import com.karaapp.karaokeapp.data.ZShPre;
import com.karaapp.karaokeapp.entity.facebook_friend.ProfileUser;
import com.karaapp.karaokeapp.entity.facebook_friend.UserProfileFBReponse;
import com.karaapp.karaokeapp.mdl.OnClick;
import com.karaapp.karaokeapp.ui.dialog.ShutdowKaraDialog;
import com.karaapp.karaokeapp.ui.fragment.BackFragment;
import com.karaapp.karaokeapp.ui.fragment.BlankFragment;
import com.karaapp.karaokeapp.ui.fragment.FriendFaceFragment;
import com.karaapp.karaokeapp.ui.fragment.SearchFragment;
import com.karaapp.karaokeapp.ui.fragment.YouTubeFragment;
import com.karaapp.karaokeapp.ui.view.BadgeCircle;
import com.karaapp.karaokeapp.ui.view.BellBadge;
import com.karaapp.karaokeapp.ui.view.IvDrawerBadge;
import com.karaapp.karaokeapp.ui.view.ProfilePictureBadge;
import com.karaapp.karaokeapp.ui.view.SettingsControl;

import org.json.JSONException;
import org.json.JSONObject;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;

import static com.karaapp.karaokeapp.ui.fragment.SearchFragment.RESULT_SPEECH;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    public boolean isRecordVideoFragment=false;

    public DrawerLayout drawerLayout;
    private static BackFragment currentFragment;
    private LoginManager btnLogin;
    private LoginButton fbLogin;
    private TextView tvNameFace;
    private Button ibFriend;
    private ProfilePictureBadge profilePictureView;
    public SearchView searchView;
    public TextView title, record_main;
    public  IvDrawerBadge mIconDrawer;
    public TextView mTittle;
    public NetworkChangeReceiver receiver;
    private Boolean bl = true;
    private TextView txtcancel, txtSave, txtPoint;
    private NavigationView navigationView;
    public ImageView imageViewMenu;
    private BadgeCircle badgeCircle;
    public BellBadge bellBadge;
    private LoginManager logMana;
    private RelativeLayout relativeLayout;
    public TextView recordButton;
    public LinearLayout layoutRecord;

    public LinearLayout layout_record;
    private static CallbackManager callbackManager = CallbackManager.Factory.create();

    public static DatabaseKara databaseKara;
    public  ImageButton ivVoice;
    public  ImageView ivback_black;
    private boolean orientation = true;
    public static final String FIELDS = "fields";
    public static final String LIST_FIELDS = "id, name, email";

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(event.getAction() == MotionEvent.ACTION_OUTSIDE){
            if(AccessToken.getCurrentAccessToken()==null)
            {
                drawerLayout.openDrawer(GravityCompat.START);

            }
            else
                callFragment(SearchFragment.newInstance());

        }
        return true;     }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        databaseKara = new DatabaseKara(this, "Karaoke.sqlite", null, 1);
        databaseKara.queryData("CREATE TABLE IF NOT EXISTS ListFavourite(videoID NVARCHAR(20) PRIMARY KEY,namesong NVARCHAR(200),duration NVARCHAR(200),linkpicture NVARCHAR(200),type NVARCHAR(200))");
        showHashKey(this);
        setContentView(R.layout.activity_main);
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        loadGui();
        setStatusBarColor();
        badgeCircle.setVisibility(View.GONE);
        checkInternet();
        layout_record = (LinearLayout) findViewById(R.id.record_land);
        layout_record.setVisibility(View.INVISIBLE);
        badgeCircle.setVisibility(View.INVISIBLE);
        record_main = (TextView) findViewById(R.id.record_main);
        imageViewMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(MainActivity.this, imageViewMenu);
                popupMenu.getMenuInflater().inflate(R.menu.menumain, popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.tophit:
                                searchView.setQuery("karaoke lyrics: karaoke online ", true);
                                ivVoice.setVisibility(View.GONE);
                                break;
                            case R.id.karaOke:
                                searchView.setQuery("karaoke lyrics: karaoke nhạc việt nam", true);
                                ivVoice.setVisibility(View.GONE);
                                break;
                            case R.id.nhactre:
                                searchView.setQuery("karaoke lyrics: nhạc trẻ", true);
                                ivVoice.setVisibility(View.GONE);
                                break;
                            case R.id.nhactrutinh:
                                searchView.setQuery("karaoke lyrics: nhạc trữ tình", true);
                                ivVoice.setVisibility(View.GONE);
                                break;
                            case R.id.nhacthieunhi:
                                searchView.setQuery("karaoke lyrics: nhạc thiếu nhi", true);
                                ivVoice.setVisibility(View.GONE);
                                break;
                            case R.id.nhacdo:
                                searchView.setQuery("karaoke lyrics: nhạc đỏ", true);
                                ivVoice.setVisibility(View.GONE);
                                break;
                            case R.id.nhacquehuong:
                                searchView.setQuery("karaoke lyrics: nhạc quê hương", true);
                                ivVoice.setVisibility(View.GONE);
                                break;
                            case R.id.rap:
                                searchView.setQuery("karaoke lyrics: nhạc rap", true);
                                ivVoice.setVisibility(View.GONE);
                                break;
                            case R.id.karaOketienganh:
                                searchView.setQuery("karaoke lyrics: karaoke english", true);
                                ivVoice.setVisibility(View.GONE);
                                break;
                        }
                        return true;
                    }
                });
                popupMenu.show();
            }
        });

        btnLogin = LoginManager.getInstance();
        btnLogin.registerCallback(callbackManager, callback);
        btnLogin.logOut();

        ivback_black = (ImageView) findViewById(R.id.ic_arrow_back_black_24dp);
        ivback_black.setVisibility(View.INVISIBLE);

        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivVoice.setVisibility(View.VISIBLE);
                searchView.setMaxWidth(Integer.MAX_VALUE);
                mTittle.setVisibility(View.GONE);

                ivVoice.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "en-US");

                        try{
                            startActivityForResult(intent, RESULT_SPEECH);
                            searchView.setQuery("", true);

                            }
                        catch (ActivityNotFoundException e){
                            Toast.makeText(MainActivity.this,
                                    "Opps! Your device doesn't support Speech to Text",
                                    Toast.LENGTH_SHORT).show();
                            }

                    }
                });
                SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
                searchView.setFocusable(true);
                searchView.setFocusableInTouchMode(true);
                searchView.onActionViewExpanded();
                searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
                mIconDrawer.setVisibility(View.INVISIBLE);
                ivback_black.setVisibility(View.VISIBLE);
                ivback_black.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        searchView.onActionViewCollapsed();
                        ivVoice.setVisibility(View.INVISIBLE);
                        title.setVisibility(View.VISIBLE);
                        ivback_black.setVisibility(View.INVISIBLE);
                        mIconDrawer.setVisibility(View.VISIBLE);
                        mIconDrawer.setOnClickListener(new View.OnClickListener() {
                               @Override
                               public void onClick(View view) {
                                   drawerAction(null);
                               }
                           }
                        );
                    }
                });
            }
        });
        ivback_black.setVisibility(View.GONE);
        mIconDrawer.setVisibility(View.VISIBLE);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchView.setQuery("", false);
                searchView.clearFocus();
                searchView.setIconified(true);
                searchView.setSubmitButtonEnabled(true);
                mIconDrawer.setVisibility(View.VISIBLE);
                mTittle.setVisibility(View.VISIBLE);
                ivVoice.setVisibility(View.INVISIBLE);

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        showHashKey(this);

        final CardView cardView = (CardView) findViewById(R.id.card);
        final Button button = (Button) findViewById(R.id.ibMoiBanBe);
        button.setOnTouchListener(new View.OnTouchListener() {
            ObjectAnimator o1 = ObjectAnimator.ofFloat(cardView, "cardElevation", 2, 8)
                    .setDuration
                            (80);
            ObjectAnimator o2 = ObjectAnimator.ofFloat(cardView, "cardElevation", 8, 2)
                    .setDuration
                            (80);

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        o1.start();
                        break;
                    case MotionEvent.ACTION_CANCEL:
                    case MotionEvent.ACTION_UP:
                        o2.start();
                        break;
                }
                return false;
            }
        });
    }

    private FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new
                    MainActivity.GraphResponse(loginResult.getAccessToken()));
            Bundle parameters = new Bundle();
            parameters.putString(FIELDS, LIST_FIELDS);
            request.setParameters(parameters);
            request.executeAsync();
        }

        @Override
        public void onCancel() {
        }

        @Override
        public void onError(FacebookException error) {
        }
    };

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
//        orientation = false;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        AppEventsLogger.activateApp(getApplication());
    }

    public  void callFragment(Fragment fragment) {
        if (fragment instanceof BackFragment) {
            currentFragment = (BackFragment) fragment;
        } else {
            currentFragment = null;
        }
        searchView.setVisibility(View.VISIBLE);
        imageViewMenu.setVisibility(View.VISIBLE);
        bellBadge.setVisibility(View.INVISIBLE);
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.frame_layout, fragment).addToBackStack(null);
        transaction.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppEventsLogger.activateApp(getApplication());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        callbackManager.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){
            case RESULT_SPEECH: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> text = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    searchView.setQuery(text.get(0), true);
                }
                break;
            }
            case 1000:
                if (resultCode == SettingsControl.LANGUAGE_CHANGED) {
                    startActivity(new Intent(this, MainActivity.class));
                    finish();
                }
                break;
        }
    }

    @OnClick
    public void drawerAction(View v) {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        if ((drawerLayout.isDrawerOpen(GravityCompat.START) && AccessToken.getCurrentAccessToken()==null)) {
            drawerLayout.openDrawer(GravityCompat.START);
//            callFragment(BlankFragment.newInstance());
            Toast.makeText(MainActivity.this, "Mời bạn đăng nhập facebook để sử dụng ứng dụng", Toast.LENGTH_SHORT).show();
        }
        if ((drawerLayout.isDrawerOpen(GravityCompat.START) && AccessToken.getCurrentAccessToken()!=null)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
        else {
            drawerLayout.openDrawer(GravityCompat.START);

        }
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            if (currentFragment != null) {
                currentFragment.onBack();
            } else {
                YouTubeFragment.isPlayer.pause();
                callFragment(SearchFragment.newInstance());
            }
        }
    }

    private void loadGui() {
        relativeLayout = (RelativeLayout) findViewById(R.id.relative);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(getApplication());
            FacebookSdk.setIsDebugEnabled(true);
            FacebookSdk.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        View hView = navigationView.getHeaderView(0);
        tvNameFace = (TextView) findViewById(R.id.id_user);
        title = (TextView) findViewById(R.id.title);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        profilePictureView = (ProfilePictureBadge) findViewById(R.id.avata);
        bellBadge = (BellBadge) findViewById(R.id.bell_badge);
        badgeCircle = (BadgeCircle) findViewById(R.id.badge_circle);
        badgeCircle.setVisibility(View.INVISIBLE);
        bellBadge.setVisibility(View.INVISIBLE);
        imageViewMenu = (ImageView) findViewById(R.id.imageViewMenu);

        ibFriend = (Button) findViewById(R.id.ibMoiBanBe);
        ibFriend.setVisibility(View.GONE);
        profilePictureView.setNumberBadge(0);
        fbLogin = (LoginButton) findViewById(R.id.loginface);

        fbLogin.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
        fbLogin.setCompoundDrawablePadding(0);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        searchView = (SearchView) findViewById(R.id.layout_Seach);
        mIconDrawer = (IvDrawerBadge) findViewById(R.id.iv_drawer);
        mTittle = (TextView) findViewById(R.id.title);
        EditText searchEditText = (EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchEditText.setTextColor(Color.parseColor("#FFFFFF"));
        searchEditText.setHintTextColor(Color.parseColor("#FFFFFF"));
        ivVoice = (ImageButton) findViewById(R.id.iv_voice);
        ivVoice.setVisibility(View.INVISIBLE);

    }

    public void setImageDrawer(int drawer) {
        mIconDrawer.setImageDrawer(drawer);
    }

    public void setTitle(String title) {
        mTittle.setText(title);
    }

    public void setClickDrawer(View.OnClickListener onClick) {
        mIconDrawer.setOnClickListener(onClick);
    }

    public void setBellFocus(boolean isFocus) {
        bellBadge.setFocus(isFocus);
    }

    public static void showHashKey(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(
                    "com.karaapp.karaokeapp", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.i("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
        } catch (NoSuchAlgorithmException e) {
        }
    }

    ProfileUser profileUser = new ProfileUser();
    private class GraphResponse implements GraphRequest.GraphJSONObjectCallback {
        private AccessToken accessToken;

        protected GraphResponse (AccessToken accessToken) {
            this.accessToken = accessToken;
        }

        @Override
        public void onCompleted(JSONObject jsonObject, com.facebook.GraphResponse response) {
            setProfileToView(jsonObject);

            Gson gson = new Gson();
            UserProfileFBReponse userProfileFBReponse = gson.fromJson(jsonObject.toString(),UserProfileFBReponse.class);

            profileUser.setToken(accessToken.getToken());
            profileUser.setTimeExpires(accessToken.getExpires());
            profileUser.setDisplayName(userProfileFBReponse.getName());
            profileUser.setIdUser(userProfileFBReponse.getId());
            profileUser.setEmail(userProfileFBReponse.getEmail());

            String objTokenUserMdl = gson.toJson(profileUser);
            ZShPre.gIns(MainActivity.this).setProfileUser(objTokenUserMdl);
            tvNameFace.setText(profileUser.getDisplayName());
//            profilePictureView.setProfileId(profileUser.getIdUser());
            profilePictureView.setProfileId(ProfileUser
                    .getIns(MainActivity.this).getIdUser());
            callFragment(SearchFragment.newInstance());
        }
    }

    private void setProfileToView(JSONObject jsonObject) {
        try {
            ivVoice.setVisibility(View.GONE);
            ibFriend.setVisibility(View.VISIBLE);
            fbLogin.setVisibility(View.INVISIBLE);
            ibFriend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callFragment(FriendFaceFragment.newInstance());
                    drawerAction(null);
                }
            });

//            ibFriend.setOnLongClickListener(new View.OnLongClickListener() {
//                @Override
//                public boolean onLongClick(View view) {
//                    AlphaAnimation animation1 = new AlphaAnimation(0.0f, 1.0f);//0.0f là độ mờ cao nhất(không thấy gì cả), 1.0f là độ mờ nhỏ nhất(chữ bình thường)
//                    animation1.setDuration(5000);// thời gian chữ bị mờ 5000 = 5 giây
//                    animation1.setStartOffset(1000);// thời gian lặp lại(nếu có cho lặp)
//                    animation1.setRepeatCount(1);// Số lần lặp lại
//                    ibFriend.startAnimation(animation1);
//                    return false;
//                }
//            });

            SharedPreferences pre = getSharedPreferences("my_data", MODE_PRIVATE);
            SharedPreferences.Editor edit = pre.edit();
            edit.putString("idProfile", jsonObject.getString("id"));
            edit.putString("nameProfile", jsonObject.getString("name"));
            edit.commit();
            SharedPreferences preferences=  getSharedPreferences("my_data", MODE_PRIVATE);
            String bchk=preferences.getString("idProfile", "");
            String user=preferences.getString("nameProfile", "");
            String unicodeString = getUnicodeString(escapeUnicodeText(user));
            String resultUnicode = unicodeString.replace("\\u", "\\U");

            try {
                user = URLEncoder.encode(user, "UTF-8");

            } catch (Exception e) {
                e.printStackTrace();
            }
            if(bchk!= null && user != null){
                InsertDataStringRequest(Config.ROOT_URL+"insertUser?idProfile="+bchk+"&nameProfile="+user);
            }
            // vào trang cá nhân face
            profilePictureView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                  /*  callFragment(TrangCaNhanFaceFragment.newInstance());
                    drawerAction(null);*/
                }
            });
            bellBadge.setNumberBadge(99);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    private void InsertDataStringRequest(String url){
        RequestQueue requestQueue= Volley.newRequestQueue(this);
        StringRequest mStringRequest=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(mStringRequest);

    }
    private final void setStatusBarColor() {
        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(this,R.color.colorAccent));
        }
    }

    public String escapeUnicodeText(String input) {

        StringBuilder b = new StringBuilder(input.length());

        java.util.Formatter f = new java.util.Formatter(b);

        for (char c : input.toCharArray()) {
            if (c < 128) {
                b.append(c);
            } else {
                f.format("\\u%04x", (int) c);
            }
        }

        return b.toString();
    }
    public String getUnicodeString(String myString) {
        String text = "";
        try {

            byte[] utf8Bytes = myString.getBytes("UTF8");
            text = new String(utf8Bytes, "UTF8");

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            e.printStackTrace();
        }
        return text;
    }
    public void broadcastIntent(View view){
       /* Intent intent = new Intent();
        if(!isOnline(this)){
            intent.setAction("com.tutorialspoint.CUSTOM_INTENT");
            sendBroadcast(intent);
        }*/
    }
    public boolean isOnline(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        //should check null because in airplane mode it will be null
        return (netInfo != null && netInfo.isConnected());
    }
    public void checkInternet() {
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        receiver = new NetworkChangeReceiver();
        registerReceiver(receiver, filter);
        bl = receiver.is_connected();
        ConnectivityManager cm = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo wifi = cm.getActiveNetworkInfo();

        if (receiver.is_connected()) {
            if (AccessToken.getCurrentAccessToken() != null) {
                logMana = LoginManager.getInstance();
                logMana.registerCallback(callbackManager, callback);
                logMana.logOut();
                logMana.logInWithPublishPermissions(MainActivity.this, Arrays
                        .asList(Config.LIST_PUBLISH_PERMISSTION_FACEBOOK));
            }
            callFragment(SearchFragment.newInstance());

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            unregisterReceiver(receiver);
        } catch (Exception e) {

        }
    }

}