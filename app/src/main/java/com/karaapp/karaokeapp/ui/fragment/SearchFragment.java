package com.karaapp.karaokeapp.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.facebook.AccessToken;
import com.karaapp.karaokeapp.R;
import com.karaapp.karaokeapp.data.Resource;
import com.karaapp.karaokeapp.entity.callback.IQueryCallback;
import com.karaapp.karaokeapp.entity.callback.IQueryDuratioCallback;
import com.karaapp.karaokeapp.mdl.VideoMdl;
import com.karaapp.karaokeapp.data.Config;
import com.karaapp.karaokeapp.ui.activity.MainActivity;
import com.karaapp.karaokeapp.ui.adapter.VideoAdapter;
import com.karaapp.karaokeapp.ui.dialog.CustomProgressDialog;
import com.karaapp.karaokeapp.ui.dialog.ShutdowKaraDialog;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class SearchFragment extends BackFragment {
    private VideoAdapter mVideoAdapter;
    public static List<VideoMdl> mVideos = new ArrayList<>();
    private List<String> mVideos3 = new ArrayList<>();
    private RecyclerView mRecyclerViewVideo;
    private RecyclerView.LayoutManager linearLayoutManager;
    protected static String urlId, urlVideos, duration;
    private AsyncHttpClient client, client1;

    private RequestParams params, paramDuration;
    private SearchView searchView;
    public static final int RESULT_SPEECH = 1;
    private MainActivity activity;
    private View view;
    private Context context;
    private CustomProgressDialog dialog;
    private RequestQueue requestQueue;
    protected static List<String> durationList = new ArrayList<>();
    protected static List<String> durationListConvert = new ArrayList<>();
    private static final int NUMBER_RESULT = 25;
    private StringBuilder builderVideoId = new StringBuilder();
    TextView tv;

    public SearchFragment() {
    }

    public static final SearchFragment newInstance() {
        SearchFragment f = new SearchFragment();
        Bundle bdl = new Bundle();
        f.setArguments(bdl);
        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (MainActivity) getActivity();

        requestQueue = Volley.newRequestQueue(getContext());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        activity.setImageDrawer(R.drawable.ic_view_headline_black_24dp);
        activity.setTitle("KaraOke Online");
        view = inflater.inflate(R.layout.fragment_search, container, false);

        activity.setClickDrawer(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.drawerAction(null);
            }
        });
        if (AccessToken.getCurrentAccessToken() == null)
            activity.drawerLayout.openDrawer(GravityCompat.START);
        else {
            tv = (TextView) view.findViewById(R.id.listview_string_empty);
            client = new AsyncHttpClient();
            params = new RequestParams();
            paramDuration = new RequestParams();
            client1 = new SyncHttpClient();
            searchView = (SearchView) getActivity().findViewById(R.id.layout_Seach);
            mRecyclerViewVideo = (RecyclerView) view.findViewById(R.id.rv_video);
            mVideoAdapter = new VideoAdapter(mVideos);
            mRecyclerViewVideo.setOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                }

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                }
            });
            int orientation = this.getResources().getConfiguration().orientation;
            if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                linearLayoutManager = new GridLayoutManager(getActivity().getApplicationContext(), 2, GridLayoutManager.VERTICAL, false);
            } else
                linearLayoutManager = new GridLayoutManager(getActivity().getApplicationContext(), 1, GridLayoutManager.VERTICAL, false);

            mRecyclerViewVideo.setLayoutManager(linearLayoutManager);
            mRecyclerViewVideo.setAdapter(mVideoAdapter);

            // final String querynhactre = query("karaoke lyrics:nhạc trẻ");
            query("karaoke lyrics:nhạc nhẹ", new IQueryCallback() {
                @Override
                public void callback(List<String> listId) {
                    builderVideoId = new StringBuilder();
                    for (String id : listId) {
                        if (builderVideoId.length() < 1) {
                            builderVideoId.append(id + "");
                        } else {
                            builderVideoId.append("," + id);
                        }
                    }

                    AsyncTaskDuration duration = new AsyncTaskDuration();
                    duration.execute(builderVideoId.toString());
                }
            });

            searchView.setVisibility(View.VISIBLE);
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String stringQuery) {
                    activity.ivVoice.setVisibility(View.VISIBLE);

                    if (stringQuery.toString().length() <= 0) {
                        searchView.onActionViewCollapsed();

                    } else if (stringQuery.toString().length() > 0) {
                        tv.setText("");
                        mVideos.clear();
                        query("karaoke lyrics:" + stringQuery, new IQueryCallback() {
                            @Override
                            public void callback(List<String> listId) {
                                builderVideoId = new StringBuilder();
                                for (String id : listId) {
                                    if (builderVideoId.length() < 1) {
                                        builderVideoId.append(id + "");
                                    } else {
                                        builderVideoId.append("," + id);
                                    }
                                }
                                // updateEmptyView();
                                AsyncTaskDuration duration = new AsyncTaskDuration();
                                duration.execute(builderVideoId.toString());
                            }
                        });
                        activity.mTittle.setVisibility(View.VISIBLE);
                        searchView.onActionViewCollapsed();
                        activity.ivVoice.setVisibility(View.GONE);
                        activity.ivback_black.setVisibility(View.GONE);
                        activity.mIconDrawer.setVisibility(View.VISIBLE);
                    }
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    return false;
                }
            });


            mVideoAdapter.setOnItemClickListener(new VideoAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(View itemView, int position) {
         /*           VideoMdl mVideo = mVideos.get(position);
                    RecordVideoFragment fragment = RecordVideoFragment.newInstance(mVideo.getVideoId(), mVideo.getTitle(), mVideo.getDurations(), mVideo.getUrl());
                    getFragmentManager().beginTransaction().replace(R.id.frame_layout, fragment).addToBackStack(null).commit();
*/
                }
            });
        }

        return view;
    }

    private void query(final String query, final IQueryCallback callback) {
        dialog = new CustomProgressDialog(activity);
        dialog.setTitle("Thông báo");
        dialog.setTitle("Loading...");
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        params.put("part", "snippet");
        params.put("type", "video");
        params.put("order", "viewCount");
        params.put("key", Config.SEARCH_API_KEY);
        params.put("maxResults", NUMBER_RESULT);
        params.put("q", query);
        urlId = "https://www.googleapis.com/youtube/v3/search";
        client.get(urlId, params, new TextHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                mVideos3.clear();
                try {
                    JSONObject data = new JSONObject(responseString);
                    JSONArray array = data.getJSONArray("items");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject obj = array.getJSONObject(i);
                        JSONObject id = obj.getJSONObject("id");
                        String videoId = id.getString("videoId");
                        JSONObject snippet = obj.getJSONObject("snippet");
                        String title = snippet.getString("title");
                        JSONObject thumbnails = snippet.getJSONObject("thumbnails");
                        JSONObject default1 = thumbnails.getJSONObject("default");
                        String url = default1.getString("url");
                        mVideos3.add(videoId);  // Lấy id để phục vụ lần search lần thứ 2 -> duration
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                callback.callback(mVideos3);
                activity.ivVoice.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(getActivity(), "Not found video you want", Toast.LENGTH_LONG);
            }
        });
    }

    public void queryDuration(String listVideoId, final IQueryDuratioCallback duratioCallback) {
        paramDuration.put("id", listVideoId);
        paramDuration.put("part", "snippet,contentDetails,statistics");
        paramDuration.put("key", Config.SEARCH_API_KEY);
        urlVideos = "https://www.googleapis.com/youtube/v3/videos";
        client1.get(urlVideos, paramDuration, new TextHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                durationList.clear();
                mVideos.clear();
                try {
                    JSONObject object = new JSONObject(responseString);
                    JSONArray Item = object.getJSONArray("items");
                    for (int i = 0; i < Item.length(); i++) {
                        JSONObject item = Item.getJSONObject(i);
                        String videoId = item.getString("id");
                        JSONObject contentDetails = item.getJSONObject("contentDetails");
                        JSONObject snippet = item.getJSONObject("snippet");
                        String title = snippet.getString("title");
                        JSONObject thumbnails = snippet.getJSONObject("thumbnails");
                        JSONObject default1 = thumbnails.getJSONObject("default");
                        String url = default1.getString("url");
                        String duration = contentDetails.getString("duration");
                        durationList.add(duration);
                        mVideos.add(new VideoMdl(videoId, title, url, duration));
                    }
                    duratioCallback.callback(durationList);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }
        });
    }

    private void showDialog() {
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    private void hideDialog() {
        if (!dialog.isShowing()) {
            dialog.hide();
            dialog.dismiss();
        }
        dialog.dismiss();
        dialog.hide();
    }

    public static String parseDuration(String in) {
        boolean hasSeconds = in.indexOf('S') > 0;
        boolean hasMinutes = in.indexOf('M') > 0;

        String s;
        if (hasSeconds) {
            s = in.substring(2, in.length() - 1);
        } else {
            s = in.substring(2, in.length());
        }

        String minutes = "0";
        String seconds = "00";

        if (hasMinutes && hasSeconds) {
            String[] split = s.split("M");
            minutes = split[0];
            seconds = split[1];
        } else if (hasMinutes) {
            minutes = s.substring(0, s.indexOf('M'));
        } else if (hasSeconds) {
            seconds = s;
        }
        // pad seconds with a 0 if less than 2 digits
        if (seconds.length() == 1) {
            seconds = "0" + seconds;
        }
        return minutes + ":" + seconds;
    }

    private class AsyncTaskDuration extends AsyncTask<String, Void, String> {


        public AsyncTaskDuration() {
        }

        @Override
        protected String doInBackground(String... params) {
            queryDuration(params[0], new IQueryDuratioCallback() {
                @Override
                public void callback(List<String> durations) {
                    durationList = durations;
                }
            });
            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            hideDialog();
            durationListConvert.clear();
            for (String str : durationList) {
                str = parseDuration(str);
                durationListConvert.add(str);
            }

            for (int i = 0; i < mVideos.size(); i++) {
                if (mVideos.get(i) != null) {
                    mVideos.get(i).setDurations(durationListConvert.get(i));
                } else {
                    break;
                }
            }
            mVideoAdapter = new VideoAdapter(mVideos);
            mRecyclerViewVideo.setAdapter(mVideoAdapter);
//            mVideoAdapter.notifyDataSetChanged();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case RESULT_SPEECH: {
                if (requestCode == activity.RESULT_OK && null != data) {
                    ArrayList<String> text = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    searchView.setQuery(text.get(0), true);
                }
                break;
            }
        }
    }

    public void callFragment(Fragment fragment) {

        searchView.setVisibility(View.VISIBLE);
        activity.imageViewMenu.setVisibility(View.VISIBLE);
        activity.bellBadge.setVisibility(View.INVISIBLE);
        FragmentManager manager = activity.getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.frame_layout, fragment).addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onBack() {
        super.onBack();
        if(activity.isRecordVideoFragment==true){
            YouTubeFragment.isPlayer.pause();
            activity.callFragment(SearchFragment.newInstance());
        }
        else {
        ShutdowKaraDialog shutdowKaraDialog = new ShutdowKaraDialog(activity);
        shutdowKaraDialog.setCancelable(false);
        shutdowKaraDialog.setCanceledOnTouchOutside(false);
        shutdowKaraDialog.setTitle(Resource.getInstance(activity).getString(R
                .string.tat_ung_dung));
        shutdowKaraDialog.show();
        }
        activity.isRecordVideoFragment=false;


    }
}
