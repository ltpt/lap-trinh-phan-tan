package com.karaapp.karaokeapp.ui.dialog;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.view.Gravity;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.karaapp.karaokeapp.R;
import com.karaapp.karaokeapp.ui.activity.MainActivity;

/**
 * Created com.karaapp.karaokeapp.ui.dialog on 8/23/2017.
 * Author: NgaNQ
 */
public class CustomProgressDialog extends Dialog {
    private ImageView imageview;

    private MainActivity activity;

    public CustomProgressDialog(@NonNull Context context) {
        super(context, R.style.TransparentProgressDialog);
        this.activity = (MainActivity) context;
        WindowManager.LayoutParams windowmanger = getWindow().getAttributes();
        windowmanger .gravity = Gravity.CENTER_HORIZONTAL;
        getWindow().setAttributes(windowmanger );
        setTitle(null);
        setCancelable(false);
        setOnCancelListener(null);
        LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams params = new  LinearLayout.LayoutParams(80, 80);
        imageview = new ImageView(context);
        imageview.setBackgroundResource(R.drawable.progress_animation);
        layout.addView(imageview, params);
        addContentView(layout, params);    }


    @Override
    public void show() {
        super.show();
        AnimationDrawable frameAnimation = (AnimationDrawable)imageview.getBackground();
        frameAnimation.start();
    }
}
