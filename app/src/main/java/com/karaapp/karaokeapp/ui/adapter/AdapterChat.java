package com.karaapp.karaokeapp.ui.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.login.widget.ProfilePictureView;
import com.karaapp.karaokeapp.R;
import com.karaapp.karaokeapp.mdl.InfoChat;

import java.util.List;

/**
 * Created com.karaapp.karaokeapp.ui.adapter on 7/3/2017.
 * Author: NgaNQ
 */
public class AdapterChat extends RecyclerView.Adapter<AdapterChat.ViewHolder> {
    private List<InfoChat> cardExchangeMdls;
    private Context context;
    private int[] mUsernameColors;
    public AdapterChat(Context context, List<InfoChat> cardExchangeMdls){
        this.context = context;
        this.cardExchangeMdls = cardExchangeMdls;
        mUsernameColors = context.getResources().getIntArray(R.array.username_colors);
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layout = -1;
        switch (viewType) {
            case InfoChat.TYPE_MESSAGE:
                layout = R.layout.listitemchat;
                break;
            case InfoChat.TYPE_LOG:
                layout = R.layout.item_log;
                break;
            case InfoChat.TYPE_ACTION:
                layout = R.layout.item_action;
                break;
        }
        View v = LayoutInflater
                .from(parent.getContext())
                .inflate(layout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        InfoChat card = cardExchangeMdls.get(position);
        if(holder.profilePictureView !=null)
        holder.profilePictureView.setProfileId(card.getFacebookID());
        if(TextUtils.isEmpty(card.getName()))
            holder.messages.setText(Html.fromHtml("<font color=\""+getUsernameColor(card.getMessages())+"\"> "+card.getMessages() + " </font>" ));

        else if(TextUtils.isEmpty(card.getMessages()))
        {
            holder.messages.setText(Html.fromHtml("<font color=\""+getUsernameColor(card.getName())+"\"> "+card.getName() + " </font>"));
        }
        else
            holder.messages.setText(Html.fromHtml("<font color=\""+getUsernameColor(card.getName())+"\"> "+card.getName() + " </font>" +" : "+ card.getMessages()));

    }

    @Override
    public int getItemViewType(int position) {
        return cardExchangeMdls.get(position).getmType();

    }

    @Override
    public int getItemCount() {
        return cardExchangeMdls.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private ProfilePictureView profilePictureView;
        private TextView messages;

        public ViewHolder(final View itemView) {
            super(itemView);
            profilePictureView = (ProfilePictureView) itemView.findViewById(R.id.facebook_picture);
            messages = (TextView) itemView.findViewById(R.id.messages_text);

        }
    }
    private int getUsernameColor(String username) {
        int hash = 7;
        for (int i = 0, len = username.length(); i < len; i++) {
            hash = username.codePointAt(i) + (hash << 5) - hash;
        }
        int index = Math.abs(hash % mUsernameColors.length);
        return mUsernameColors[index];
    }

}
