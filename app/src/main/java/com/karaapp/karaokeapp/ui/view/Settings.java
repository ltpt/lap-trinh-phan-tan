package com.karaapp.karaokeapp.ui.view;

import android.content.Context;
import android.content.SharedPreferences;

public class Settings {
    public static final String PREFS_NAME = "twoc_settings_example";
    public static final String KEY_SETTINGS_AVAILABLE = "settingsavailable";

    public static final String KEY_STATE_VIBRATION = "vibrationstate";

    private SharedPreferences prefAccessor;

    public Settings(Context parentContext) {
        prefAccessor = parentContext.getSharedPreferences(PREFS_NAME, 0);

        if(prefAccessor.contains(KEY_SETTINGS_AVAILABLE)) {
            SharedPreferences.Editor editor = prefAccessor.edit();

            editor.putString(KEY_SETTINGS_AVAILABLE, "true");

            if(prefAccessor.contains(KEY_STATE_VIBRATION)) {
                editor.putString(KEY_STATE_VIBRATION, "Ngon_Ngu");
            }

            editor.apply();
        }
    }

    public String getSettingValue(String settingKeyName) {
        return prefAccessor.getString(settingKeyName, "");
    }

    public void setSettingValue(String settingKeyName, String valueToInsert) {
        SharedPreferences.Editor editor = prefAccessor.edit();
        editor.putString(settingKeyName, valueToInsert);
        editor.apply();
    }

    public boolean isVibrationOn() {
        boolean returnValue = false;
        if(prefAccessor.getString(KEY_STATE_VIBRATION, "").equals("Ngon_Ngu")) {
            returnValue = true;
        }
        return returnValue;
    }
}
