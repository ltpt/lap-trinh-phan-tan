package com.karaapp.karaokeapp.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Base64;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import com.facebook.AccessToken;
import com.karaapp.karaokeapp.ui.activity.MainActivity;
import com.karaapp.karaokeapp.ui.fragment.ChatFragment;
import com.karaapp.karaokeapp.mdl.NavMenuMdl;
import com.karaapp.karaokeapp.mdl.NavTittleHeader;
import com.karaapp.karaokeapp.R;
import com.karaapp.karaokeapp.ui.adapter.NavMenuAdapter;
import com.karaapp.karaokeapp.data.Resource;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Nganq on 4/18/2017.
 */

public class ListviewNavMenu extends ExpandableListView {

    private MainActivity activity;
    List<NavTittleHeader> listDataHeader;
    HashMap<NavTittleHeader, List<NavMenuMdl>> listDataChild;

    public ListviewNavMenu(Context context) {
        super(context);
        this.activity = (MainActivity) context;
        init();
    }

    public ListviewNavMenu(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.activity = (MainActivity) context;
        init();

    }

    public ListviewNavMenu(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.activity = (MainActivity) context;
        init();

    }

    public void init() {
        listDataChild = new HashMap<NavTittleHeader, List<NavMenuMdl>>();
        listDataHeader = new ArrayList<>();
        String canhan=Resource.getInstance(activity).getString(R.string.person);
        String congdong=Resource.getInstance(activity).getString(R.string.community);
        String ungdung=Resource.getInstance(activity).getString(R.string.app);
        String   encoded = null,correctDecoded = null;
        String   encoded1 = null,correctDecoded1 = null;
        String   encoded2 = null,correctDecoded2 = null;

        try {
            encoded = URLEncoder.encode(canhan, "UTF-8");
            correctDecoded = URLDecoder.decode(encoded, "UTF-8");
            encoded1 = URLEncoder.encode(congdong, "UTF-8");
            correctDecoded1 = URLDecoder.decode(encoded1, "UTF-8");
            encoded2 = URLEncoder.encode(ungdung, "UTF-8");
            correctDecoded2 = URLDecoder.decode(encoded2, "UTF-8");

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        listDataHeader.add(new NavTittleHeader(correctDecoded));
        //listDataHeader.add(new NavTittleHeader(correctDecoded1));
        //listDataHeader.add(new NavTittleHeader(correctDecoded2));

        List<NavMenuMdl> listNavMenuPerson = new ArrayList();
        listNavMenuPerson.add(new NavMenuMdl(R.drawable.thamgiaface, Resource.getInstance(activity).getString(R.string.community_fb)));

        /*listNavMenuPerson.add(new NavMenuMdl(R.drawable.thuam_mp3, Resource.getInstance(activity).getString(R.string.list_record_mp3)));
        listNavMenuPerson.add(new NavMenuMdl(R.drawable.thuam_video, Resource.getInstance(activity).getString(R.string.list_record_vid)));
        listNavMenuPerson.add(new NavMenuMdl(R.drawable.list_love, Resource.getInstance(activity).getString(R.string.list_like)));
        listNavMenuPerson.add(new NavMenuMdl(R.drawable.installation, Resource.getInstance(activity).getString(R.string.action_settings)));

        List<NavMenuMdl> listNavMenuCommunity = new ArrayList();
        listNavMenuCommunity.add(new NavMenuMdl(R.drawable.bxh_song, Resource.getInstance(activity).getString(R.string.billboard_record)));
        listNavMenuCommunity.add(new NavMenuMdl(R.drawable.ghiam, Resource.getInstance(activity).getString(R.string.billboard_singer)));
        listNavMenuCommunity.add(new NavMenuMdl(R.drawable.bxh_song, Resource.getInstance(activity).getString(R.string.song_by_date)));
        listNavMenuCommunity.add(new NavMenuMdl(R.drawable.thamgiaface, Resource.getInstance(activity).getString(R.string.community_fb)));

        List<NavMenuMdl> listNavMenuApp = new ArrayList();
        listNavMenuApp.add(new NavMenuMdl(R.drawable.gioithieu, Resource.getInstance(activity).getString(R.string.intro_app)));
        listNavMenuApp.add(new NavMenuMdl(R.drawable.cauhoi, Resource.getInstance(activity).getString(R.string.question)));

        listNavMenuApp.add(new NavMenuMdl(R.drawable.game, Resource.getInstance(activity).getString(R.string.game_app)));
        listNavMenuApp.add(new NavMenuMdl(R.drawable.dieukhoan, Resource.getInstance(activity).getString(R.string.rule_call)));

        */
        listDataChild.put(listDataHeader.get(0), listNavMenuPerson);
        /*listDataChild.put(listDataHeader.get(1), listNavMenuCommunity);
        listDataChild.put(listDataHeader.get(2), listNavMenuApp);
        */
        NavMenuAdapter adapter = new NavMenuAdapter(activity, listDataHeader, listDataChild);
        setAdapter(adapter);
        for (int i = 0; i < adapter.getGroupCount(); i++) {
            expandGroup(i);
        }

        setOnChildClickListener(new OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                if(AccessToken.getCurrentAccessToken()==null)
                {}
                else {
                    if (groupPosition == 0) {

                        if (childPosition == 0) {
                            activity.callFragment(ChatFragment.newInstance());
                            activity.setTitle("Chat");
                        }
                        /*if (childPosition == 1) {
                            activity.callFragment(RecordListVideoFragment.newInstance());
                            activity.setTitle(Resource.getInstance(activity).getString(R.string.list_record_vid));
                        }
                        if (childPosition == 2) {
                            activity.callFragment(DsYeuThichFragment.newInstance());
                            activity.setTitle(Resource.getInstance(activity).getString(R.string.list_like));
                        }
                        if (childPosition == 3) {
                            activity.callFragment(CaiDatFragment.newInstance());
                            activity.setTitle(Resource.getInstance(activity).getString(R.string.action_settings));
                        }*/
                    }
                    /*if (groupPosition == 1) {
                        if (childPosition == 0) {
                            activity.callFragment(BillboardSongFragment.newInstance());
                            activity.setTitle(Resource.getInstance(activity).getString(R.string.title_billboard_song));
                        }
                        if (childPosition == 1) {
                            activity.callFragment(BillboardSingerFragment.newInstance());
                            activity.setTitle(Resource.getInstance(activity).getString(R.string.billboard_singer));
                        }
                        if (childPosition == 2) {
                            activity.callFragment(ListSongByDateFragment.newInstance());
                            activity.setTitle(Resource.getInstance(activity).getString(R.string.karaokedate_list));
                        }
                        if (childPosition == 3) {
                            activity.callFragment(ChatFragment.newInstance());
                            activity.setTitle(Resource.getInstance(activity).getString(R.string.chat_application));
*//*Intent i = new Intent(Intent.ACTION_VIEW,Uri.parse("https://www.facebook.com/")); activity.startActivity(i);*//*

                        }
                    }
                    if (groupPosition == 2) {
                        if (childPosition == 0) {
                            activity.callFragment(GioiThieuFragment.newInstance());
                            activity.setTitle(Resource.getInstance(activity).getString(R.string.intro_app));
                        }
                        if (childPosition == 1) {
                            activity.callFragment(QuetionOneLevelFragment.newInstance());
                            activity.setTitle(Resource.getInstance(activity).getString(R.string.question));
                        }
                        if (childPosition == 2) {
                            activity.callFragment(GameAndAppFragment.newInstance());
                            activity.setTitle(Resource.getInstance(activity).getString(R.string.game_app));
                        }
                        if (childPosition == 3) {
                            activity.callFragment(DieuKhoanFragment.newInstance());
                            activity.setTitle(Resource.getInstance(activity).getString(R.string.rule_call));
                        }
                    }*/
                }
                activity.drawerAction(null);
                activity.setBellFocus(false);
                return false;
            }

        });
        setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });
    }

}
