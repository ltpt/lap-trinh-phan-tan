package com.karaapp.karaokeapp.ui.fragment;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.karaapp.karaokeapp.R;
import com.karaapp.karaokeapp.data.Config;
import com.karaapp.karaokeapp.data.Resource;
import com.karaapp.karaokeapp.mdl.InfoChat;
import com.karaapp.karaokeapp.ui.activity.MainActivity;
import com.karaapp.karaokeapp.ui.adapter.AdapterChat;
import com.karaapp.karaokeapp.ui.dialog.ServerOffDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatFragment extends BackFragment {
    private Gson gson;
    private TextView tv;
    private MainActivity activity;
    private View v;
    private static final int TYPING_TIMER_LENGTH = 600;
    private boolean mTyping = false;
    private Boolean isConnected = true;
    private Socket mSocket;
    private Handler mTypingHandler = new Handler();
    private EditText messagesInput;
    private List<InfoChat> list = new ArrayList<>();
    private RecyclerView.LayoutManager linearLayoutManager;
    private RecyclerView mRecyclerViewChat;
    private ImageButton btnSend;
    private boolean isClick = false;
    private AdapterChat adapterChat;
    private String bchk, user;

    public ChatFragment() {
    }

    public static final ChatFragment newInstance() {
        ChatFragment f = new ChatFragment();
        Bundle bdl = new Bundle();
        f.setArguments(bdl);
        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (MainActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_chat, container, false);

        adapterChat = new AdapterChat(getActivity(), list);
        loadGUI();
        activity.setTitle(Resource.getInstance(activity).getString(R.string.chat_application));

        messagesInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_PREVIOUS || event.getAction() == KeyEvent.KEYCODE_ENTER) {
                    // do something, e.g. set your TextView here via .setText()
                    InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });
        try {
            mSocket = IO.socket(Config.ROOT_URL_CHAT);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        SharedPreferences preferences = activity.getSharedPreferences("my_data", MODE_PRIVATE);
        bchk = preferences.getString("idProfile", "");
        user = preferences.getString("nameProfile", "");
        mSocket.connect();
        messagesInput.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

            }
        });
        //if (mSocket.connected()) {
        if (bchk != null && user != null) {

            GetData(Config.ROOT_URL + "getChat");
            messagesInput.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    btnSend.setEnabled(true);
                    tv.setVisibility(View.GONE);
                    if (!mTyping) {
                        mTyping = true;
                        mSocket.emit("typing");
                    }
                    mTypingHandler.removeCallbacks(onTypingTimeout);
                    mTypingHandler.postDelayed(onTypingTimeout, TYPING_TIMER_LENGTH);


                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            activity.searchView.setVisibility(View.INVISIBLE);
            activity.imageViewMenu.setVisibility(View.INVISIBLE);
            activity.ivVoice.setVisibility(View.INVISIBLE);
            activity.setClickDrawer(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    activity.callFragment(SearchFragment.newInstance());
                }
            });
            mSocket.on(Socket.EVENT_CONNECT, onRetrieveDataServerConnection);
            mSocket.on("typing", onTyping);
            mSocket.on("stop typing", onStopTyping);
            mSocket.on("user left", onUserLeft);

            mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
            mSocket.emit("loginServer", user);
            btnSend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    btnSend.setEnabled(true);
                    mTyping = false;
                    isClick = true;
                    if (isClick && TextUtils.isEmpty(messagesInput.getText().toString()) || messagesInput.getText().toString().trim().matches("")) {
                        btnSend.setEnabled(false);
                        Toast.makeText(activity, R.string.string_empty, Toast.LENGTH_SHORT).show();

                    } else {
                        //1 phat tin hieu
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("idProfile", bchk);
                            jsonObject.put("nameUser", user);
                            jsonObject.put("messages", messagesInput.getText().toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        mSocket.emit("client-send-messages", jsonObject);
                        messagesInput.setText("");
                    }
                }
            });
            mSocket.on("server-send-messages_receiver", onRetrieveDataServerSend);
            mSocket.on("listMessages", onRetrieveListMessages);
        } else Toast.makeText(activity, "Mời đăng nhập nick facebook", Toast.LENGTH_SHORT).show();
        return v;
    }

    private Emitter.Listener onRetrieveListMessages = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            JSONObject object = (JSONObject) args[0];
            String profile = null, name = null, messages = null;
            try {
                profile = object.getString("listmessages");
               /* name = json.getString("nameUser");
                messages = json.getString("messages");
*/
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };
    private Emitter.Listener onRetrieveDataServerSend = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String object = (String) args[0];
                    String profile = null, name = null, messages = null;
                    JSONObject json = null;
                    try {
                        json = new JSONObject(object);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    try {
                        profile = json.getString("idProfile");
                        name = json.getString("nameUser");
                        messages = json.getString("messages");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    removeTyping(name);
                    addMessage(profile, name, messages);
                }
            });

        }
    };
    private Emitter.Listener onRetrieveDataNumUser = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject object = (JSONObject) args[0];
            try {
                int numUser = object.getInt("numUsers");
                addParticipantsLog(numUser);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };
    private Emitter.Listener onRetrieveDataServerConnection = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            if (!isConnected) {
                if (null != bchk) {
                    mSocket.on("addUser", onRetrieveDataNumUser);
                }

                Toast.makeText(getActivity().getApplicationContext(),
                        R.string.connect, Toast.LENGTH_LONG).show();
                isConnected = true;
            }
        }
    };
    private Emitter.Listener onUserLeft = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String username;
                    int numUsers;
                    try {
                        username = data.getString("username");
                        numUsers = data.getInt("numUsers");
                    } catch (JSONException e) {
                        Toast.makeText(activity, getResources().getString(R.string.error), Toast.LENGTH_SHORT).show();
                        return;
                    }

                    addLog(getResources().getString(R.string.message_user_left, username));
                    addParticipantsLog(numUsers);
                    removeTyping(username);
                }
            });
        }
    };
    /*  private Emitter.Listener onConnectReturn = new Emitter.Listener() {
          @Override
          public void call(final Object... args) {
              getActivity().runOnUiThread(new Runnable() {
                  @Override
                  public void run() {
                      String object = (String) args[0];
                      String profile = null, name = null, messages = null;
                      JSONArray json = null;
                      try {
                          json = new JSONArray(object);
                      } catch (JSONException e) {
                          e.printStackTrace();
                      }
                      for(int i=0;i<json.length();i++){
                          JSONObject jsonobject= null;
                          try {
                              jsonobject = (JSONObject) json.get(i);
                              profile=jsonobject.getString("idProfile");
                              name=jsonobject.getString("nameProfile");
                              messages=jsonobject.getString("messages");
                          } catch (JSONException e) {
                              e.printStackTrace();
                          }
                          list.add(new InfoChat(InfoChat.TYPE_MESSAGE, profile, name, messages));
                      }
                      adapterChat.notifyItemInserted(list.size() - 1);

                     *//* try {
                        profile = json.getString("idProfile");
                        name = json.getString("nameUser");
                        messages = json.getString("messages");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }*//*
                }
            });
        }
    };*/
    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ServerOffDialog dialog = new ServerOffDialog(activity);
                    dialog.show();
                }
            });
        }
    };

    private Emitter.Listener onStopTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String username;
                    try {
                        username = data.getString("username");
                    } catch (JSONException e) {
                        Toast.makeText(activity, getResources().getString(R.string.error), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    removeTyping(username);
                }
            });
        }
    };
    private Emitter.Listener onTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String username;
                    try {
                        username = data.getString("username");
                    } catch (JSONException e) {
                        Toast.makeText(activity, getResources().getString(R.string.error), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    addTyping(username);
                }
            });
        }
    };
    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    isConnected = false;
                    Toast.makeText(getActivity().getApplicationContext(),
                            R.string.disconnect, Toast.LENGTH_LONG).show();
                }
            });
        }
    };

    private void loadGUI() {
        tv = (TextView) v.findViewById(R.id.tvwelcome);
        messagesInput = (EditText) v.findViewById(R.id.message_input);
        btnSend = (ImageButton) v.findViewById(R.id.send_button);
        mRecyclerViewChat = (RecyclerView) v.findViewById(R.id.list_conversation);
        linearLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        mRecyclerViewChat.setLayoutManager(linearLayoutManager);
        mRecyclerViewChat.setHasFixedSize(true);
        mRecyclerViewChat.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        mRecyclerViewChat.setItemAnimator(new DefaultItemAnimator());
        mRecyclerViewChat.setAdapter(adapterChat);
        // addLog(getResources().getString(R.string.message_welcome));


    }

    private void addLog(String message) {
        list.add(new InfoChat(InfoChat.TYPE_LOG, null, null, message));
        adapterChat.notifyItemInserted(list.size() - 1);
        scrollToBottom();
    }

    private void GetData(String url) {
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        /*JsonArrayRequest(int method, String url, JSONArray jsonRequest, Listener<JSONArray> listener, ErrorListener errorListener)*/
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                gson = new Gson();
                InfoChat[] infoChats = gson.fromJson(response.toString(), InfoChat[].class);
                for (InfoChat infoChat : infoChats) {
                    list.add(infoChat);
                }
                adapterChat.notifyDataSetChanged();
                scrollToBottom();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ServerOffDialog dialog = new ServerOffDialog(activity);
                dialog.show();
            }
        });
        requestQueue.add(jsonArrayRequest);
    }

    private void addParticipantsLog(int numUsers) {
        addLog(getResources().getQuantityString(R.plurals.message_participants, numUsers, numUsers));
    }

    private void addMessage(String facebookID, String username, String message) {
        list.add(new InfoChat(InfoChat.TYPE_MESSAGE, facebookID, username, message));
        adapterChat.notifyItemInserted(list.size() - 1);
        scrollToBottom();
    }

    private void addTyping(String username) {
        list.add(new InfoChat(InfoChat.TYPE_ACTION, null, username, null));
        adapterChat.notifyItemInserted(list.size() - 1);
        scrollToBottom();
    }

    private void removeTyping(String username) {
        for (int i = list.size() - 1; i >= 0; i--) {
            InfoChat message = list.get(i);
            if (message.getmType() == InfoChat.TYPE_ACTION && message.getName().equals(username)) {
                list.remove(i);
                adapterChat.notifyItemRemoved(i);
            }
        }
    }

    private void scrollToBottom() {
        mRecyclerViewChat.scrollToPosition(adapterChat.getItemCount() - 1);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSocket.disconnect();

        mSocket.off(Socket.EVENT_CONNECT, onRetrieveDataServerConnection);
        mSocket.off(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.off(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.off("server-send-messages_receiver", onRetrieveDataServerSend);
        mSocket.off("addUser", onRetrieveDataNumUser);
        mSocket.off("user left", onUserLeft);
        mSocket.off("typing", onTyping);
        mSocket.off("stop typing", onStopTyping);

    }

    @Override
    public void onBack() {
        super.onBack();
        activity.callFragment(SearchFragment.newInstance());
    }

    private Runnable onTypingTimeout = new Runnable() {
        @Override
        public void run() {
            if (!mTyping) return;

            mTyping = false;
            mSocket.emit("stop typing");
        }
    };
}
