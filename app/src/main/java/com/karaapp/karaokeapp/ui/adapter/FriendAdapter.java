package com.karaapp.karaokeapp.ui.adapter;

import android.content.Context;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.facebook.login.widget.ProfilePictureView;
import com.karaapp.karaokeapp.R;
import com.karaapp.karaokeapp.entity.facebook_friend.IFriendMdl;

import java.util.List;

/**
 * Created by ThanhThuy on 01/07/2017.
 */

public class FriendAdapter extends BaseAdapter {
    private List<IFriendMdl> list;
    private Context context;

    public FriendAdapter(Context context, List<IFriendMdl> list) {
        this.list = list;
        this.context = context;
    }
    public List<IFriendMdl> getList() {
        return list;
    }


    public void setList(List<IFriendMdl> list) {
        this.list = list;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if(convertView == null){
            convertView = View.inflate(context, R.layout.friend_adapter,null);
            holder = new ViewHolder() ;
            holder.avatar = (ImageView) convertView.findViewById(R.id.iv_avata);
            holder.tvName = (TextView) convertView.findViewById(R.id.name);
            holder.ivCheck = (ImageView) convertView.findViewById(R.id.check);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }
        IFriendMdl mdl = list.get(position);
        Glide.with(holder.avatar.getContext()).load(mdl.getAvatar()).into(holder.avatar);
        holder.tvName.setText(mdl.getName());

        if(mdl.getCheck()){
            holder.ivCheck.setImageResource(R.drawable.checked);
        }else {
            holder.ivCheck.setImageResource(R.drawable.check);
        }
        return convertView;
    }

    private class ViewHolder {
        private ImageView avatar;
        private TextView tvName;
        private ImageView ivCheck;
    }
}
