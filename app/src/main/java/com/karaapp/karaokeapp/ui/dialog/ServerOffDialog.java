package com.karaapp.karaokeapp.ui.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StyleRes;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.karaapp.karaokeapp.R;
import com.karaapp.karaokeapp.data.Resource;
import com.karaapp.karaokeapp.ui.activity.MainActivity;

/**
 * Created com.karaapp.karaokeapp.ui.dialog on 8/23/2017.
 * Author: NgaNQ
 */
public class ServerOffDialog extends KaraDialog {
    private MainActivity activity;

    public ServerOffDialog(@NonNull Context context) {
        super(context);
        this.activity = (MainActivity) context;

    }

    public ServerOffDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        this.activity = (MainActivity) context;
    }

    @Override
    public View generateContent(LinearLayout linearLayout, LayoutInflater layoutInflater) {
        View view = layoutInflater.inflate(R.layout.shutdown_content,
                linearLayout);
        TextView tvContent = (TextView) view.findViewById(R.id.shutdown_content);
        tvContent.setText(Resource.getInstance(activity).getString(R.string.server_bao_tri));
        return view;
    }

    @Override
    public View generateFooter(LinearLayout linearLayout, LayoutInflater layoutInflater) {
        View view = layoutInflater.inflate(R.layout.server_off_footer, linearLayout);
        Button btCancel = (Button) view.findViewById(R.id.bt_cancel_app);

        btCancel.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(1);
            }
        });
        return view;
    }
}
