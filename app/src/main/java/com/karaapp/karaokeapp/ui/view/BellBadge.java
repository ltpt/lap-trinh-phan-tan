package com.karaapp.karaokeapp.ui.view;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.karaapp.karaokeapp.R;

public class BellBadge extends RelativeLayout {

    private BadgeCircle badgeCircle;
    private ImageView bell;
    private Context context;

    public BellBadge(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public BellBadge(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    public BellBadge(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public void setNumberBadge(int numberBadge) {
        if (numberBadge == 0) {
            badgeCircle.setVisibility(View.INVISIBLE);
        } else {
            badgeCircle.setVisibility(View.VISIBLE);
            badgeCircle.setNumberBadge(numberBadge);
        }
    }

    public void setFocus(boolean isFocus){
        if (isFocus) {
            bell.setColorFilter(ContextCompat.getColor(context, R.color.colorOrange));
        } else {
            bell.setColorFilter(ContextCompat.getColor(context,android.R.color.black));
        }

    }

    private void init() {
        inflate(getContext(), R.layout.bell_badge, this);
        this.badgeCircle = (BadgeCircle) findViewById(R.id.badge_circle);
        bell = (ImageView) findViewById(R.id.bell);
        this.badgeCircle.setVisibility(View.INVISIBLE);
    }
}